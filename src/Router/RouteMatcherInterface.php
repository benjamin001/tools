<?php

namespace kylin\Tools\Router;

interface RouteMatcherInterface
{
    public function match(string $requestUri, string $route): bool;
}