<?php

include 'vendor/autoload.php';

use kylin\Tools\Router\Router;
use kylin\Tools\Router\ExactRouteMatcher;
use kylin\Tools\Router\RouteBuilder;

# 启动： php -S 0.0.0.0:80 src/Router/Demo/index.php

class HomeController
{
    public function index()
    {
        echo '欢迎来到主页！';
    }

    public function profile()
    {
        echo '用户资料页面';
    }
}

class userMiddleware
{
    public function admin($next)
    {
        echo '应用了管理员中间件<br>';
        $next();
    }

    public function user($next)
    {
        echo '应用了用户中间件<br>';
        $next();
    }
}


$router = new Router(new ExactRouteMatcher());

// 定义路由组
// 创建中间件实例
$userMiddleware = new UserMiddleware();

$router->group('/admin', function (RouteBuilder $builder) use ($userMiddleware) {
    $builder->middleware(function ($next) use ($userMiddleware) {
        $userMiddleware->admin($next);
    });

    $builder->group('/user', function (RouteBuilder $builder) {
        $builder->middleware([userMiddleware::class, 'user']);

        $builder->get('/profile', [HomeController::class, 'index']);
    });

//    $builder->get('/dashboard', function () {
//        echo '管理员仪表盘';
//    });
});

// 定义无分组路由
$router->get('/home', [HomeController::class, 'index']);
// 解析并处理当前请求
$router->dispatch();
//print_r($router->getAllRoute());