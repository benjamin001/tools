<?php

namespace kylin\Tools\Router;

class Common
{

    /**
     * @param $handler
     * @return array [参数不可执行，执行结果]
     */
    public static function handlerExec($handler): array
    {
        if (is_callable($handler)) {
            return [true, $handler()];
        }
        if (is_array($handler) && count($handler) === 2) {
            [$class, $method] = $handler;
            if (class_exists($class) && method_exists($class, $method)) {
                return [true, (new $class())->$method()];
            }
        }
        return [false, false];
    }
}