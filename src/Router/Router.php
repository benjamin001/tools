<?php

namespace kylin\Tools\Router;

class Router
{
    private array $routes = [];
    private RouteMatcherInterface $routeMatcher;

    public function __construct(RouteMatcherInterface $routeMatcher)
    {
        $this->routeMatcher = $routeMatcher;
    }

    public function registerRoutes(RouteBuilder $builder): void
    {
        $routes = $builder->build();
        foreach ($routes as $method => $methodRoutes) {
            foreach ($methodRoutes as $route => $info) {
                $this->addRoute($method, $route, $info['handler'], $info['middlewares']);
            }
        }
    }

    public function addRoute(string $method, string $route, $handler, array $middlewares): void
    {
        $this->routes[$method][$route] = [
            'handler' => $handler,
            'middlewares' => $middlewares
        ];
    }

    public function getAllRoute(): array
    {
        return $this->routes;
    }

    public function dispatch(): void
    {
        $requestMethod = $_SERVER['REQUEST_METHOD'];
        $requestUri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $requestUri = rtrim($requestUri, '/');

        if (isset($this->routes[$requestMethod])) {
            foreach ($this->routes[$requestMethod] as $route => $info) {
                if ($this->routeMatcher->match($requestUri, $route)) {
                    $middlewareChain = new MiddlewareChain($info['middlewares']);
                    $middlewareChain->handle(function () use ($info) {
                        $handler = $info['handler'];
                        [$paramRes, $execRes] = Common::handlerExec($handler);
                        if ($paramRes === false) {
                            throw new \RuntimeException(' 404 Not Found', 404);
                        }
                    });
                    return;
                }
            }
        }
        throw new \RuntimeException(' 404 Not Found', 404);
    }

    public function group(string $prefix, callable $callback): void
    {
        $builder = new RouteBuilder($prefix);
        $callback($builder);
        $this->registerRoutes($builder);
    }

    public function get(string $route, $handler): void
    {
        // 简化外部调用方式，直接使用 Router 添加 GET 路由
        $builder = new RouteBuilder();
        $builder->get($route, $handler);
        $this->registerRoutes($builder);
    }

    public function post(string $route, $handler): void
    {
        // 简化外部调用方式，直接使用 Router 添加 POST 路由
        $builder = new RouteBuilder();
        $builder->post($route, $handler);
        $this->registerRoutes($builder);
    }
}