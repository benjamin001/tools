<?php

namespace kylin\Tools\Router;

class MiddlewareChain implements MiddlewareInterface
{
    private array $middlewares;

    public function __construct(array $middlewares)
    {
        $this->middlewares = $middlewares;
    }

    public function handle(callable $handler): void
    {
        $current = $handler;
        foreach ($this->middlewares as $middleware) {
            $current = function () use ($middleware, $current) {
                if (is_array($middleware) && count($middleware) === 2) {
                    [$class, $method] = $middleware;
                    if (class_exists($class) && method_exists($class, $method)) {
                        (new $class())->$method($current);
                    }
                } else {
                    // 如果是函数或闭包
                    $middleware($current);
                }
            };
        }
        $current();
    }
}
