<?php

namespace kylin\Tools\Router;

interface MiddlewareInterface
{
    public function handle(callable $handler): void;
}