<?php

namespace kylin\Tools\Router;

class RouteBuilder
{
    private string $prefix;
    private array $middlewares = [];
    private array $routes = [];

    public function __construct(string $prefix = '', array $middlewares = [])
    {
        $this->prefix = rtrim($prefix, '/');
        $this->middlewares = $middlewares;
    }

    public function middleware($middleware): self
    {
        $this->middlewares[] = $middleware;
        return $this;
    }

    public function addRoute(string $method, string $route, $handler): self
    {
        $fullRoute = $this->prefix . '/' . ltrim($route, '/');
        $this->routes[$method][$fullRoute] = [
            'handler' => $handler,
            'middlewares' => $this->middlewares
        ];
        return $this;
    }

    public function get(string $route, $handler): self
    {
        return $this->addRoute('GET', $route, $handler);
    }

    public function post(string $route, $handler): self
    {
        return $this->addRoute('POST', $route, $handler);
    }

    public function group(string $prefix, callable $callback): self
    {
        // 新建一个 RouteBuilder 实例，用于处理组内路由
        $builder = new RouteBuilder($this->prefix  . $prefix,$this->middlewares);
        $callback($builder);
        $routes = $builder->build();
        foreach ($routes as $method => $methodRoutes) {
            foreach ($methodRoutes as $route => $info) {
                $this->routes[$method][$route] = $info;
            }
        }

        return $this;
    }

    public function build(): array
    {
        return $this->routes;
    }
}
