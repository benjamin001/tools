<?php

namespace kylin\Tools\Router;

class ExactRouteMatcher implements RouteMatcherInterface
{
    public function match(string $requestUri, string $route): bool
    {
        return $requestUri === $route;
    }
}