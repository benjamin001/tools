<?php

namespace kylin\Tools\Router;

class RegexRouteMatcher implements RouteMatcherInterface
{
    public function match(string $requestUri, string $route): bool
    {
        $pattern = '#^' . preg_replace('#\{[^\}]+\}#', '([^/]+)', $route) . '$#';
        return preg_match($pattern, $requestUri);
    }
}