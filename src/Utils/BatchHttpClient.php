<?php

namespace kylin\Tools\Utils;

class BatchHttpClient
{
    private array $curlHandles = [];

    private \CurlMultiHandle $multiCurlHandle;

    // 初始化cURL资源
    private function initCurlHandles(array $requests): void
    {
        $this->multiCurlHandle = curl_multi_init();

        foreach ($requests as $key => $request) {
            $url = $request['url'];
            $this->curlHandles[$key] = curl_init($url);
            curl_setopt($this->curlHandles[$key], CURLOPT_RETURNTRANSFER, true);

            // 设置请求方法
            if (isset($request['method'])) {
                switch (strtoupper($request['method'])) {
                    case 'POST':
                        curl_setopt($this->curlHandles[$key], CURLOPT_POST, true);
                        if (isset($request['data'])) {
                            curl_setopt($this->curlHandles[$key], CURLOPT_POSTFIELDS, $request['data']);
                        }
                        break;
                    case 'PUT':
                        curl_setopt($this->curlHandles[$key], CURLOPT_CUSTOMREQUEST, 'PUT');
                        if (isset($request['data'])) {
                            curl_setopt($this->curlHandles[$key], CURLOPT_POSTFIELDS, $request['data']);
                        }
                        break;
                    case 'DELETE':
                        curl_setopt($this->curlHandles[$key], CURLOPT_CUSTOMREQUEST, 'DELETE');
                        break;
                    // Default is GET
                    default:
                        curl_setopt($this->curlHandles[$key], CURLOPT_HTTPGET, true);
                        break;
                }
            }

            // 设置请求头
            if (isset($request['headers'])) {
                curl_setopt($this->curlHandles[$key], CURLOPT_HTTPHEADER, $request['headers']);
            }

            // 添加到多重cURL句柄
            curl_multi_add_handle($this->multiCurlHandle, $this->curlHandles[$key]);
        }
    }

    // 执行所有cURL请求
    private function executeRequests(): void
    {
        $running = null;

        do {
            curl_multi_exec($this->multiCurlHandle, $running);
            usleep(100000); // Sleep for 100ms to allow the cURL handles to progress
        } while ($running > 0);
    }

    // 关闭所有cURL资源
    private function closeHandles(): void
    {
        foreach ($this->curlHandles as $handle) {
            curl_multi_remove_handle($this->multiCurlHandle, $handle);
            curl_close($handle);
        }

        curl_multi_close($this->multiCurlHandle);
    }

    // 发起批量请求并返回响应
    public function batchRequest(array $requests): array
    {
        $this->initCurlHandles($requests);
        $this->executeRequests();

        $responses = [];
        foreach ($this->curlHandles as $key => $handle) {
            $response = curl_multi_getcontent($handle);
            $responses[$key] = $response;
        }

        $this->closeHandles();

        return $responses;
    }
}