<?php

namespace kylin\Tools\Utils;

class NumberUtil
{
    /**
     * 科学记数法转数字
     * 3500000 可以表示为 3.5*10的6次方
     * 0.000045 可以表示为 4.5*10的负5次方
     * 科学记数法通常以类似于 1.23e5 或 2.45E-3 的形式表示，其中 e 或 E 后面的数字表示指数。
     * 例如，1.23e5 表示1.23*10的五次方，而2.45E-3 表示2.45*10的负三次方
     * @param string $number
     * @return string|null
     */
    public static function scToNum(string $number): ?string
    {
        if (stripos($number, 'e') === false) {
            return $number;
        }
        if (!preg_match("/^([\\d.]+)[eE]([\\d\\-\\+]+)$/", str_replace(array(" ", ","), "", trim($number)), $matches)) {
            return $number;
        }
        $data = preg_replace(array("/^[0]+/"), "", rtrim($matches[1], "0."));
        if ($data === null) {
            return $number;
        }
        $length = (int)$matches[2];
        if ($data[0] === ".") {
            $data = "0{$data}";
        }

        if ($length === 0) {
            return $data;
        }

        $dot_position = strpos($data, ".");
        if ($dot_position === false) {
            $dot_position = strlen($data);
        }
        $data = str_replace(".", "", $data);
        if ($length > 0) {
            $repeat_length = $length - (strlen($data) - $dot_position);
            if ($repeat_length > 0) {
                $data .= str_repeat('0', $repeat_length);
            }
            $dot_position += $length;
            $data = ltrim(substr($data, 0, $dot_position), "0") . "." . substr($data, $dot_position);
        } elseif ($length < 0) {
            $repeat_length = abs($length) - $dot_position;
            if ($repeat_length > 0) {
                $data = str_repeat('0', $repeat_length) . $data;
            }
            $dot_position += $length;//此处length为负数，直接操作
            if ($dot_position < 1) {
                $data = ".{$data}";
            } else {
                $data = substr($data, 0, $dot_position) . "." . substr($data, $dot_position);
            }
        }
        if ($data[0] === ".") {
            $data = "0{$data}";
        }

        return trim($data, ".");
    }

    /**
     * 转科学技术法
     * @param string $number
     * @return string
     */
    public static function numToSc(string $number): string
    {
        // 使用 sprintf 获取科学记数法字符串
        $tmp1 = explode('.', $number);

        $decimalCount = isset($tmp1[1]) ? strlen($tmp1[1]) : 0;
        $precision = (strlen($tmp1[0]) + $decimalCount) - 1;
        $scientificNotation = strtolower(sprintf("%.{$precision}e", $number));
        //移除除无效0
        $tmp = explode('e', $scientificNotation);
        $tmp[0] = rtrim($tmp[0], '0');
        $scientificNotation = implode('e', $tmp);


        // 获取指数部分
        preg_match('/e([+-]?\d+)$/', $scientificNotation, $matches);
        $exponent = (int)($matches[1] ?? 0);

        // 去掉指数部分的无效零
        $exponentPart = 'e' . ($exponent < 0 ? $exponent : '+' . $exponent);
        return str_replace($exponentPart, 'e' . $exponent, $scientificNotation);
    }

    /**
     *
     * 二进制转十进制（php原函数：bindec）
     * @param string $binary
     * @return int
     */
    public static function binaryToDecimal(string $binary): int
    {
        $decimal = 0;
        $length = strlen($binary);
        // 从右到左遍历二进制字符串
        for ($i = 0; $i < $length; $i++) {
            // 获取当前位的二进制数字
            $bit = $binary[$length - $i - 1];
            // 将其转换为十进制并累加
            if ($bit === '1') {
                $decimal += 2 ** $i;
            }
        }
        return $decimal;
    }

    /**
     * 十进制转二进制（php原函数：decbin）
     * @param int $decimal
     * @return string
     */
    public static function decimalToBinary(int $decimal): string
    {
        if ($decimal === 0) {
            return '0';
        }

        $binary = '';

        while ($decimal > 0) {
            $binary = ($decimal % 2) . $binary; // 将当前余数作为二进制位
            $decimal = intdiv($decimal, 2); // 将十进制数除以 2，获取商
        }

        return $binary;
    }

    /**
     * 二进制转十六进制(php原函数：先转10：bindec，再转16：dechex)
     * @param string $binary
     * @return string
     */
    public static function binaryToHexManual(string $binary): string
    {
        $binary = str_pad($binary, ceil(strlen($binary) / 4) * 4, '0', STR_PAD_LEFT); // 补齐到4的倍数

        $hexDigits = '0123456789ABCDEF'; // 十六进制字符

        $hex = '';
        $length = strlen($binary);

        for ($i = 0; $i < $length; $i += 4) {
            $binSegment = substr($binary, $i, 4); // 获取4位的二进制段

            // 计算该二进制段对应的十六进制数值
            $decimalValue = 0;
            for ($j = 0; $j < 4; $j++) {
                $decimalValue = $decimalValue * 2 + ($binSegment[$j] == '1' ? 1 : 0);
            }

            // 获取对应的十六进制字符
            $hex .= $hexDigits[$decimalValue];
        }
        return $hex;
    }

    /**
     * 十六进制转十进制
     * @param string $hex
     * @return float|int
     */
    public static function hexToDecimal(string $hex): float|int
    {
        // 去掉前缀 '0x'
        $hex = str_replace('0x', '', $hex);

        // 十六进制字符到十进制值的映射
        $hexDigits = '0123456789ABCDEF';
        $hex = strtoupper($hex); // 转换为大写以统一处理

        $decimal = 0;
        $length = strlen($hex);

        for ($i = 0; $i < $length; $i++) {
            $char = $hex[$i];
            // 查找字符在十六进制字符集中的位置
            $value = strpos($hexDigits, $char);
            if ($value === false) {
                throw new \InvalidArgumentException("Invalid hex character: $char");
            }
            // 计算该字符的十进制值，并加到结果中
            $decimal = $decimal * 16 + $value;
        }

        return $decimal;
    }
}