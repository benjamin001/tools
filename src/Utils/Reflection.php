<?php

namespace kylin\Tools\Utils;

use ReflectionMethod;
use ReflectionUnionType;

class Reflection
{

    /**
     * 获取方法参数信息
     * @param object $instance
     * @param string $methodName
     * @return array
     */
    public function getMethodParametersInfo(object $instance, string $methodName): array
    {
        try {
            $reflectionMethod = new ReflectionMethod($instance, $methodName);
        } catch (\ReflectionException $e) {
            return [];
        }
        $parametersInfo = [];
        foreach ($reflectionMethod->getParameters() as $param) {
            $paramName = $param->getName();
            $paramType = $param->getType();
            $defaultValue = $param->isDefaultValueAvailable() ? $param->getDefaultValue() : null;

            $types = [];
            if ($paramType instanceof ReflectionUnionType) {
                foreach ($paramType->getTypes() as $type) {
                    $types[] = $type->getName() . ($type->allowsNull() ? ' | null' : '');
                }
            } else {
                $types[] = ($paramType ? $paramType->getName() : 'mixed') . ($paramType && $paramType->allowsNull() ? ' | null' : '');
            }
            $parametersInfo[] = [
                'name' => $paramName,
                'types' => $types,
                'default' => $defaultValue,
            ];
        }
        return $parametersInfo;
    }

}