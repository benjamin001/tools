<?php

namespace kylin\Tools\Utils;

class ObjectUtil
{
    /**
     * @param string $className
     * @param array $data
     * @return object|bool
     */
    public static function createObjectFromData(string $className, array $data): object|bool
    {
        try {
            if (!class_exists($className)) {
                return false;
            }

            $reflectionClass = new \ReflectionClass($className);
            $object = $reflectionClass->newInstanceWithoutConstructor();

            foreach ($data as $key => $value) {
                if ($reflectionClass->hasProperty($key)) {
                    $property = $reflectionClass->getProperty($key);
                    $property->setAccessible(true);

                    // 获取属性类型
                    $type = $property->getType();
                    if ($type instanceof \ReflectionUnionType) {
                        // 处理联合类型
                        $propertyValue = null;

                        foreach ($type->getTypes() as $unionType) {
                            if ($unionType instanceof \ReflectionNamedType) {
                                $typeName = $unionType->getName();

                                if (class_exists($typeName) && is_array($value)) {
                                    // 尝试实例化对象数组
                                    $propertyValue = [];
                                    foreach ($value as $item) {
                                        $objectItem = self::createObjectFromData($typeName, $item);
                                        if ($objectItem !== false) {
                                            $propertyValue[] = $objectItem;
                                        }
                                    }
                                    break; // 成功实例化一个类型就结束循环
                                }
                            }
                        }

                        if ($propertyValue !== null) {
                            $property->setValue($object, $propertyValue);
                        }
                    } elseif ($type instanceof \ReflectionNamedType && class_exists($type->getName())) {
                        // 处理单一命名类型
//                        if (is_array($value)) {
//                            // 尝试实例化对象数组
//                            $propertyValue = [];
//                            foreach ($value as $item) {
//                                $objectItem = self::createObjectFromData($type->getName(), $item);
//                                if ($objectItem !== false) {
//                                    $propertyValue[] = $objectItem;
//                                }
//                            }
//                            if (count($propertyValue) === 1) {
//                                // 如果数组只包含一个对象，直接赋值给属性
//                                $property->setValue($object, $propertyValue[0]);
//                            } elseif (count($propertyValue) > 1) {
//                                // 如果数组包含多个对象，抛出异常或处理错误情况
//                                // 这里可以根据实际需求进行处理
//                                throw new \InvalidArgumentException("Expected single object value for property '{$key}'");
//                            }
//                        } else {
//
//                        }
                        // 尝试实例化单一对象
                        $propertyValue = self::createObjectFromData($type->getName(), $value);
                        if ($propertyValue !== false) {
                            $property->setValue($object, $propertyValue);
                        }
                    } else {
                        // 直接赋值给对象属性
                        $property->setValue($object, $value);
                    }
                }
            }

            return $object;
        } catch (\ReflectionException $e) {
            return false;
        }
    }

}