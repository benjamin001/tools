<?php

namespace kylin\Tools\Utils;
class TimeUtil
{
    /**
     * 毫秒时间戳
     * @return string
     */
    public static function milliseconds(): string
    {
        return bcmul(microtime(true) . '', '1000');
    }

    /**
     * 微秒时间戳
     * @return string
     */
    public static function microseconds(): string
    {
        return bcmul(microtime(true) . '', '1000000');
    }
}