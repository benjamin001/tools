<?php

namespace kylin\Tools\Http;

class HttpRequest
{
    public string $url;
    public string $method = 'GET';
    public ?array $data = [];
    public ?array $headers;


    public function setUrl(string $url): HttpRequest
    {
        $this->url = $url;
        return $this;
    }


    public function setMethod(string $method = 'GET'): HttpRequest
    {
        $this->method = $method;
        return $this;
    }

    public function setData(array $data): HttpRequest
    {
        $this->data = $data;
        return $this;
    }

    public function setHeaders(array $headers): HttpRequest
    {
        $this->headers = $headers;
        return $this;
    }


    public function getFormattedData(): ?string
    {
        if (empty($this->data)) {
            return null;
        }

        // 判断 Content-Type 是否为 JSON
        $isJson = false;
        if ($this->headers !== null) {
            foreach ($this->headers as $header) {
                if (stripos($header, 'Content-Type: application/json') !== false) {
                    $isJson = true;
                    break;
                }
            }
        }
        return $isJson ? json_encode($this->data, JSON_THROW_ON_ERROR) : http_build_query($this->data);
    }
}