<?php

namespace kylin\Tools\Database;


use kylin\Tools\Database\Entity\TableEntity;

class TableDefinition
{
    /**
     * 创建数据表.
     *
     * @param TableEntity $tableEntity 数据表定义实体
     * @return array 返回创建数据表的 SQL 语句，如果参数无效则返回 null
     */
    public function createSql(TableEntity $tableEntity): array
    {
        $tableValidator = new TableValidator();
        $error = $tableValidator->validateColumns($tableEntity);
        if ($error) {
            return [-1, $error[0]];
        }
        // 获取表名
        $tableName = $tableEntity->table_name;

        $sql = "CREATE TABLE `{$tableName}` (";

        // 获取字段定义数组
        $columns = $tableEntity->columns;

        // 获取索引定义数组
        $indexes = $tableEntity->indexes;

        // 验证并生成字段部分的 SQL
        $columnDefinitions = [];
        foreach ($columns as $column) {
            $columnName = $column->name;
            $columnType = $column->type;
            $columnLength = $column->length;
            $columnDecimal = $column->decimal;
            $notNull = $column->notNull;
            $autoIncrement = $column->auto_increment;
            $unsigned = $column->unsigned;
            $comment = $column->comment;
            $isPrimaryKey = $column->is_primary_key;
            $default = $column->default;
            $zeroFill = $column->zero_fill;
            $onUpdateCurrentTimestamp = $column->on_update_current_timestamp;

            // 构建字段定义部分的 SQL
            $columnDefinition = "`{$columnName}` {$columnType}";

            if ($columnLength !== null) {
                $columnDefinition .= "({$columnLength}";
                if ($columnDecimal !== null) {
                    $columnDefinition .= ", {$columnDecimal}";
                }
                $columnDefinition .= ')';
            }

            if ($isPrimaryKey) {
                $columnDefinition .= ' PRIMARY KEY';
            }

            if ($notNull) {
                $columnDefinition .= ' NOT NULL';
            }

            if ($autoIncrement) {
                $columnDefinition .= ' AUTO_INCREMENT';
            }

            if ($unsigned) {
                $columnDefinition .= ' UNSIGNED';
            }

            if ($default !== null) {
                if ($default === 'CURRENT_TIMESTAMP') {
                    $columnDefinition .= " DEFAULT {$default}";
                    if ($onUpdateCurrentTimestamp) {
                        $columnDefinition .= ' ON UPDATE CURRENT_TIMESTAMP';
                    }
                } else {
                    $columnDefinition .= " DEFAULT '{$default}'";
                }
            }

            if ($zeroFill && in_array($columnType, ['tinyint', 'smallint', 'mediumint', 'int', 'bigint'])) {
                $columnDefinition .= ' ZEROFILL';
            }

            if (!empty($comment)) {
                $columnDefinition .= " COMMENT '{$comment}'";
            }

            $columnDefinitions[] = $columnDefinition;
        }
        // 添加字段部分的 SQL
        $sql .= implode(',', $columnDefinitions);

        // 添加索引部分的 SQL
        foreach ($indexes as $index) {
            $indexName = $index->name;
            $indexColumns = implode(', ', $index->columns);
            $indexType = $index->type;
            $indexMethod = $index->method;
            $indexComment = $index->comment;

            $indexSql = "INDEX `{$indexName}` ({$indexColumns})";

            if ($indexType !== 'INDEX') {
                $indexSql = strtoupper($indexType) . ' ' . $indexSql;
            }

            if ($indexMethod !== 'BTREE') {
                $indexSql .= " USING {$indexMethod}";
            }
            if (!empty($indexComment)) {
                $indexSql .= " COMMENT '{$indexComment}'";
            }

            $sql .= ", {$indexSql}";
        }

        // 添加表的字符集和排序规则
        $charset = $tableEntity->charset;
        $collation = $tableEntity->collation;

        $sql .= ") ENGINE={$tableEntity->engine} DEFAULT CHARSET={$charset} COLLATE={$collation};";
        return [0, $sql];
    }
}