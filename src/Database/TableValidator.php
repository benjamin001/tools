<?php


namespace kylin\Tools\Database;

use kylin\Tools\Database\Entity\ColumnEntity;
use kylin\Tools\Database\Entity\ColumnTypesEntity;
use kylin\Tools\Database\Entity\TableEntity;

class TableValidator
{
    /**
     * 验证 TableEntity 中的 columns 字段.
     *
     * @return array 返回验证结果，如果验证通过则返回空数组，否则返回包含错误信息的数组
     */
    public function validateColumns(TableEntity $tableDefinition): array
    {
        $errors = [];

        $columns = $tableDefinition->columns;

        foreach ($columns as $column) {
            if (!$column instanceof ColumnEntity) {
                $errors[] = 'Invalid column definition. Each column must be an instance of ColumnEntity.';
                continue;
            }

            // 验证字段类型是否有效
            if (!$this->isValidColumnType($column->type)) {
                $errors[] = "Invalid column type '{$column->type}'.";
            }
            // 针对特定字段类型进行额外验证
            switch ($column->type) {
                case ColumnTypesEntity::TINYINT:
                    if (!$this->isValidTinyIntRange($column->length)) {
                        $errors[] = 'Invalid range for TINYINT column. It must be between -128 and 127.';
                    }
                    break;
                case ColumnTypesEntity::SMALLINT:
                    if (!$this->isValidSmallIntRange($column->length)) {
                        $errors[] = 'Invalid range for SMALLINT column. It must be between -32768 and 32767.';
                    }
                    break;
                case ColumnTypesEntity::MEDIUMINT:
                    if (!$this->isValidMediumIntRange($column->length)) {
                        $errors[] = 'Invalid range for MEDIUMINT column. It must be between -8388608 and 8388607.';
                    }
                    break;
                case ColumnTypesEntity::INT:
                    if (!$this->isValidIntRange($column->length)) {
                        $errors[] = 'Invalid range for INT column. It must be between -2147483648 and 2147483647.';
                    }
                    break;
                case ColumnTypesEntity::BIGINT:
                    if (!$this->isValidBigIntRange($column->length)) {
                        $errors[] = 'Invalid range for BIGINT column.';
                    }
                    break;
                case ColumnTypesEntity::DECIMAL:
                    // Add validation logic for DECIMAL type if needed
                    break;
                case ColumnTypesEntity::FLOAT:
                    // Add validation logic for FLOAT type if needed
                    break;
                case ColumnTypesEntity::DOUBLE:
                    // Add validation logic for DOUBLE type if needed
                    break;
                case ColumnTypesEntity::CHAR:
                    // Add validation logic for CHAR type if needed
                    break;
                case ColumnTypesEntity::VARCHAR:
                    // Add validation logic for VARCHAR type if needed
                    break;
                case ColumnTypesEntity::TEXT:
                    // Add validation logic for TEXT type if needed
                    break;
                case ColumnTypesEntity::ENUM:
                    // Add validation logic for ENUM type if needed
                    break;
                case ColumnTypesEntity::DATE:
                    // Add validation logic for DATE type if needed
                    break;
                case ColumnTypesEntity::TIME:
                    // Add validation logic for TIME type if needed
                    break;
                case ColumnTypesEntity::DATETIME:
                    break;
                case ColumnTypesEntity::TIMESTAMP:
                    $this->isValidTimestampFormat($column->default);
                    // Add validation logic for TIMESTAMP type if needed
                    break;
                case ColumnTypesEntity::BOOLEAN:
                    // Add validation logic for BOOLEAN type if needed
                    break;
                case ColumnTypesEntity::BINARY:
                    // Add validation logic for BINARY type if needed
                    break;
                case ColumnTypesEntity::VARBINARY:
                    // Add validation logic for VARBINARY type if needed
                    break;
                default:
                    // Handle unknown column types or custom types
                    $errors[] = "Unsupported column type '{$column->type}'.";
                    break;
            }


            if ($column->is_primary_key && $column->name !== 'id') {
                $errors[] = "The primary key must be an id field";
            }
        }

        return $errors;
    }

    /**
     * 验证字段类型是否有效.
     */
    private function isValidColumnType(string $type): bool
    {
        return in_array($type, [
            ColumnTypesEntity::TINYINT,
            ColumnTypesEntity::SMALLINT,
            ColumnTypesEntity::MEDIUMINT,
            ColumnTypesEntity::INT,
            ColumnTypesEntity::BIGINT,
            ColumnTypesEntity::DECIMAL,
            ColumnTypesEntity::FLOAT,
            ColumnTypesEntity::DOUBLE,
            ColumnTypesEntity::CHAR,
            ColumnTypesEntity::VARCHAR,
            ColumnTypesEntity::TEXT,
            ColumnTypesEntity::ENUM,
            ColumnTypesEntity::DATE,
            ColumnTypesEntity::TIME,
            ColumnTypesEntity::DATETIME,
            ColumnTypesEntity::TIMESTAMP,
            ColumnTypesEntity::BOOLEAN,
            ColumnTypesEntity::BINARY,
            ColumnTypesEntity::VARBINARY,
        ], true);
    }

    /**
     * 验证 TINYINT 字段范围是否有效.
     */
    private function isValidTinyIntRange(?int $length): bool
    {
        return $length >= -128 && $length <= 127;
    }

    /**
     * 验证 SMALLINT 字段范围是否有效.
     */
    private function isValidSmallIntRange(?int $length): bool
    {
        return $length >= -32768 && $length <= 32767;
    }

    /**
     * 验证 MEDIUMINT 字段范围是否有效.
     */
    private function isValidMediumIntRange(?int $length): bool
    {
        return $length >= -8388608 && $length <= 8388607;
    }

    /**
     * 验证 INT 字段范围是否有效.
     */
    private function isValidIntRange(?int $length): bool
    {
        return $length >= -2147483648 && $length <= 2147483647;
    }

    /**
     * 验证 BIGINT 字段范围是否有效.
     */
    private function isValidBigIntRange(?int $length): bool
    {
        // 没有具体的范围限制，留作扩展
        return true;
    }

    private function isValidTimestampFormat(?string $value): bool
    {
        if ($value === null) {
            return false; // 如果值为 null，则不合法
        }

        // 此处仅示例，假设需要验证 TIMESTAMP 格式为 Y-m-d H:i:s
        return (bool)preg_match('/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/', $value);
    }
}
