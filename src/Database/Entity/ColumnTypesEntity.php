<?php

declare(strict_types=1);

namespace kylin\Tools\Database\Entity;

/**
 * Class ColumnTypesEntity.
 *
 * 此类用于声明常见的数据库字段类型及其属性。
 */
class ColumnTypesEntity
{
    // 数值类型
    public const TINYINT = 'tinyint';   // 微小整数 (-128 到 127)
    public const SMALLINT = 'smallint'; // 小整数 (-32768 到 32767)
    public const MEDIUMINT = 'mediumint'; // 中整数 (-8388608 到 8388607)
    public const INT = 'int';           // 整数 (-2147483648 到 2147483647)
    public const BIGINT = 'bigint';     // 大整数 (-9223372036854775808 到 9223372036854775807)
    public const DECIMAL = 'decimal';   // 固定小数点数
    public const FLOAT = 'float';       // 浮点数
    public const DOUBLE = 'double';     // 双精度浮点数

    // 字符串类型
    public const CHAR = 'char';         // 定长字符串
    public const VARCHAR = 'varchar';   // 变长字符串
    public const TEXT = 'text';         // 长文本字符串
    public const ENUM = 'enum';         // 枚举类型

    // 日期时间类型
    public const DATE = 'date';         // 日期
    public const TIME = 'time';         // 时间
    public const DATETIME = 'datetime'; // 日期时间
    public const TIMESTAMP = 'timestamp'; // 时间戳

    // 布尔类型
    public const BOOLEAN = 'boolean';   // 布尔类型

    // 二进制数据类型
    public const BINARY = 'binary';     // 二进制数据
    public const VARBINARY = 'varbinary'; // 变长二进制数据
}
