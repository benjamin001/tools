<?php

declare(strict_types=1);

namespace kylin\Tools\Database\Entity;


/**
 * Class IndexEntity.
 *
 * @property string $name 索引名称
 * @property array $columns 索引字段数组
 * @property string $type 索引类型，默认值为 'INDEX'
 * @property string $method 索引方法，默认值为 'BTREE'
 * @property null|string $comment 索引注释
 */
class IndexEntity
{
    /** @var string 索引名称 */
    public string $name;

    /** @var array 索引字段数组 */
    public array $columns = [];

    /** @var string 索引类型，默认为 'INDEX' */
    public string $type = 'INDEX';

    /** @var string 索引方法，默认为 'BTREE' */
    public string $method = 'BTREE';

    /** @var null|string 索引注释 */
    public ?string $comment;

    /**
     * 将索引实体转换为数组.
     */
    public function toArray(): array
    {
        $indexArray = [
            'name' => $this->name,
            'columns' => $this->columns,
            'type' => $this->type,
            'method' => $this->method,
        ];

        if (! empty($this->comment)) {
            $indexArray['comment'] = $this->comment;
        }

        return $indexArray;
    }
}
