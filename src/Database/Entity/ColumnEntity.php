<?php


namespace kylin\Tools\Database\Entity;

/**
 * Class ColumnEntity.
 *
 * @property string $name 字段名称
 * @property string $type 字段类型
 * @property null|int $length 字段长度
 * @property null|int $decimal 小数位数（用于数字类型）
 * @property bool $notNull 是否不允许为空
 * @property bool $auto_increment 是否自增
 * @property bool $unsigned 是否无符号
 * @property bool $virtual 是否虚拟字段
 * @property bool $is_primary_key 是否主键
 * @property null|string $default 默认值
 * @property bool $zero_fill 是否使用零填充（仅对整数类型字段有效）
 * @property null|string $comment 字段注释
 * @property bool $on_update_current_timestamp 根据当前时间更新（仅对时间类型字段有效）
 */
class ColumnEntity
{
    /** @var string 字段名称 */
    public string $name;

    /** @var string 字段类型 */
    public string $type;

    /** @var null|int 字段长度 */
    public ?int $length = null;

    /** @var null|int 小数位数（用于数字类型） */
    public ?int $decimal = null;

    /** @var bool 是否不允许为空 */
    public bool $notNull = false;

    /** @var bool 是否自增 */
    public bool $auto_increment = false;

    /** @var bool 是否无符号 */
    public bool $unsigned = false;

    /** @var bool 是否虚拟字段 */
    public bool $virtual = false;

    /** @var bool 是否主键 */
    public bool $is_primary_key = false;

    /** @var null|string 默认值 */
    public ?string $default = null;

    /** @var bool 是否使用零填充（仅对整数类型字段有效） */
    public bool $zero_fill = false;

    /** @var null|string 字段注释 */
    public ?string $comment = null;

    /** @var bool 根据当前时间更新（仅对时间类型字段有效） */
    public bool $on_update_current_timestamp = false;


    /**
     * 将对象转换为数组.
     */
    public function toArray(): array
    {
        $columnArray = [
            'name' => $this->name,
            'type' => $this->type,
            'length' => $this->length,
            'decimal' => $this->decimal,
            'notNull' => $this->notNull,
            'auto_increment' => $this->auto_increment,
            'unsigned' => $this->unsigned,
            'virtual' => $this->virtual,
            'is_primary_key' => $this->is_primary_key,
            'default' => $this->default,
            'zero_fill' => $this->zero_fill,
            'on_update_current_timestamp' => $this->on_update_current_timestamp,
        ];

        if (!empty($this->comment)) {
            $columnArray['comment'] = $this->comment;
        }

        return $columnArray;
    }
}
