<?php

declare(strict_types=1);

namespace kylin\Tools\Database\Entity;

/**
 *
 * @property string $table_name 表名
 * @property ColumnEntity[] $columns 列定义数组
 * @property IndexEntity[] $indexes 索引定义数组
 * @property string $engine 表的存储引擎，默认值为 'InnoDB'
 * @property string $charset 表的字符集，默认值为 'utf8mb4'
 * @property string $collation 表的排序规则，默认值为 'utf8mb4_unicode_ci'
 * @property null|string $comment 表的注释
 * @property bool $forceCreate 是否强制创建数据表，默认为 false
 */
class TableEntity
{
    /** @var string 表名 */
    public string $table_name = '';

    /** @var ColumnEntity[] 列定义数组 */
    public ColumnEntity|array $columns = [];

    /** @var IndexEntity[] 索引定义数组 */
    public IndexEntity|array $indexes = [];

    /** @var string 表的存储引擎，默认为 'InnoDB' */
    public string $engine = 'InnoDB';

    /** @var string 表的字符集，默认为 'utf8mb4' */
    public string $charset = 'utf8mb4';

    /** @var string 表的排序规则，默认为 'utf8mb4_unicode_ci' */
    public string $collation = 'utf8mb4_unicode_ci';

    /** @var null|string 表的注释 */
    public ?string $comment = null;

    /** @var bool 是否强制创建数据表，默认为 false */
    public bool $force_create = false;

    /**
     * 添加列定义到表定义实体.
     *
     * @param ColumnEntity $column 列定义对象
     */
    public function addColumn(ColumnEntity $column): void
    {
        $this->columns[] = $column;
    }

    /**
     * 添加索引定义到表定义实体.
     *
     * @param IndexEntity $index 索引定义对象
     */
    public function addIndex(IndexEntity $index): void
    {
        $this->indexes[] = $index;
    }

    /**
     * 将表定义实体转换为数组.
     */
    public function toArray(): array
    {
        $columnsArray = [];
        foreach ($this->columns as $column) {
            $columnsArray[] = $column->toArray();
        }

        $indexesArray = [];
        foreach ($this->indexes as $index) {
            $indexesArray[] = $index->toArray();
        }

        return [
            'table_name' => $this->table_name,
            'columns' => $columnsArray,
            'indexes' => $indexesArray,
            'engine' => $this->engine,
            'charset' => $this->charset,
            'collation' => $this->collation,
            'comment' => $this->comment,
            'forceCreate' => $this->forceCreate,
        ];
    }
}
