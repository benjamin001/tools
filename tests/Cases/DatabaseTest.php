<?php

namespace Cases;

use kylin\Tools\Database\Entity\TableEntity;
use kylin\Tools\Database\TableDefinition;
use kylin\Tools\Utils\ObjectUtil;
use PHPUnit\Framework\TestCase;

class DatabaseTest extends TestCase
{

    public function testCreateSql()
    {
        $user = file_get_contents(dirname(__DIR__).'/Data/users.json');
        $res  = json_decode($user,true);
        /**
         * @var TableEntity $tableEntity
         */
        $tableEntity = ObjectUtil::createObjectFromData(TableEntity::class,$res);
        $tableDefinition  = new TableDefinition();
        $createRes = $tableDefinition->createSql($tableEntity);
        $this->assertIsArray($createRes);
    }
}