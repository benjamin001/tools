<?php

namespace Cases;

use kylin\Tools\Utils\NumberUtil;
use PHPUnit\Framework\TestCase;

class NumberUtilTest extends TestCase
{
    public function testScToNum(): void
    {
        $this->assertEquals('3500000',NumberUtil::scToNum('3.5e6'));
        $this->assertEquals('0.000045',NumberUtil::scToNum('4.5e-5'));
        $this->assertEquals('123430.001',NumberUtil::scToNum('1.23430001e5'));
    }


    public function testNumToSc(): void
    {
        $this->assertEquals('3.5e6',NumberUtil::numToSc('3500000'));
        $this->assertEquals('4.5e-5',NumberUtil::numToSc('0.000045'));
        $this->assertEquals('1.23430001e5',NumberUtil::numToSc('123430.001'));
        $this->assertEquals('1.2343000000000001e5',NumberUtil::numToSc('123430.00000000001'));
    }

}