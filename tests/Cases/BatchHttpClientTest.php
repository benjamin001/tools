<?php

namespace Cases;

use kylin\Tools\Http\BatchHttpClient;
use kylin\Tools\Http\HttpRequest;
use kylin\Tools\Utils\ObjectUtil;
use PHPUnit\Framework\TestCase;

class BatchHttpClientTest extends TestCase
{

    public function testBatchRequest()
    {
        $data = [
            [
                'url' => 'http://127.0.0.1:3468/api/web/v1/coupon/list-exchange-record?' . http_build_query(['page_size' => 1, 'page' => 1]),
                'method' => 'GET',
                'data' => [],
                'headers' => ["Cookie: lang=cn; login_notice_check=%2F; exchange_rate_switch=1; defaultBuyCryptoFiat=JPY; market_title=usdt; curr_fiat=USD; _dx_uzZo5y=587c194803a881702a772500dfe5d294157dad667aab8b18f2865ce73b55cba3267c3c29; b_notify=1; is_portfolio_margin_account_1879201053=0; is_portfolio_margin_switch_status_1879201053=0; finger_print=66a846786ekuLLdK48s7GwzBh4zuKnXwO5nQqy61; finger_print_time=2024-07-30; lasturl=%2Fmyaccount%2Fcoupon_center; uid=1879042189; nickname=kane; is_on=1; pver=c2f86116bb63241344676ad17bb8391c; pver_ws=12d0d006b5c7c1bb5d11f798f1c7803a; csrftoken=306b585039514a4862534c385539425a767a2b5741574e47797244434146414d6c627668486334725a326d74664a344a486a763053637665726870386b4b6c71; is_portfolio_margin_account_1879042189=0; is_portfolio_margin_switch_status_1879042189=0; _gat_UA-1833997-40=1; _ga_JNHPQJS9Q4=GS1.4.1722323310.10.1.1722325135.60.0.0; _ga=GA1.1.2032786390.1721722841; _gid=GA1.1.314307228.1722216619; _gat_gtag_UA_1833997_38=1; _gat_gtag_UA_222583338_1=1; _ga_CF71BLYRCR=GS1.1.1722301414.7.1.1722325139.34.0.0; _ga_SNPLSCMNGD=GS1.1.1722319761.9.1.1722325139.54.0.0"],
            ]
        ];
        $requests = [];
        foreach ($data as $item) {
            $requests['list-exchange-record'] = ObjectUtil::createObjectFromData(HttpRequest::class, $item);
        }
        $client = new BatchHttpClient();
        $responses = $client->batchRequest($requests);
        print_r($responses);
        print_r($client->getErrors());
        print_r($client->getInfo());
    }
}